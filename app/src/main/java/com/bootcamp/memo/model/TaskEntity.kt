package com.bootcamp.memo.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class TaskEntity (
    @PrimaryKey(autoGenerate = true) var id : Int?,
    @ColumnInfo(name ="title") var title : String?,
    @ColumnInfo(name = "description") var description : String?
) : Parcelable