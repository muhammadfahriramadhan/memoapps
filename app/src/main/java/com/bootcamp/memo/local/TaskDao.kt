package com.bootcamp.memo.local

import androidx.room.Dao
import androidx.room.Insert
import com.bootcamp.memo.model.TaskEntity

@Dao
interface TaskDao {

    @Insert
    fun insert(task : TaskEntity) : Long

}