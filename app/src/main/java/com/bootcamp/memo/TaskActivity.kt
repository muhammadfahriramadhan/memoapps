package com.bootcamp.memo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import com.bootcamp.memo.local.TaskDatabase
import com.bootcamp.memo.model.TaskEntity
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TaskActivity : AppCompatActivity() {
    private var taskDatabase : TaskDatabase? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        taskDatabase = TaskDatabase.getInstance(this)

        setSupportActionBar(toolbar)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.menuSave ->{
                val title = edtxtTaskTitle.text.toString()
                val description = edtxDescription.text.toString()
                GlobalScope.launch {
                    val result = taskDatabase?.taskDao()?.insert(TaskEntity(null,title,description))
                    if (result != 0.toLong()){
                        runOnUiThread {
                            edtxtTaskTitle.setText("")
                            edtxDescription.setText("")
                            Toast.makeText(this@TaskActivity,"Berhasil Insert data",Toast.LENGTH_LONG).show()
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}